# docker setup for vanilla exist-db with changed admin and tei-demo password and volumes for config and data

### create file 'adminpw' to change admin password during build:
```
let $pw1 := sm:passwd('admin','xxxx')
let $pw2:= sm:passwd('tei-demo','xxxx')
return "changed"
```

## environment
```bash
export APPNAME=jurisfrisica
export VERSION=0.5
export VOLUMEROOT=${HOME}/volumes
export DOCKER_BUILDKIT=1
```
## build image:
```
download latest released tei-publisher and oas-router from http://exist-db.org/exist/apps/public-repo/index.html
docker build  --secret id=adminpw,src=adminpw -t ${APPNAME}:${VERSION} .
rm adminpw
```

## run image
see compose file for data volumes. Change xxx to your appname in the compose file you use.
```
once: id=$(docker create ${APPNAME}:${VERSION})
once: docker cp $id:/exist/data ${VOLUMEROOT}/${APPNAME}
once: docker rm -f $id
once: cp conf.xml ${VOLUMEROOT}/${APPNAME}
once: docker swarm init (due to network sometimes needs: --advertise-addr n.n.n.n)
docker stack deploy --compose-file docker-compose.yml ${APPNAME}
```
or use compose (beware compose-file provides syntax for both swarm and compose!)
