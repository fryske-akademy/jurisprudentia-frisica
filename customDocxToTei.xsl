<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
        xmlns="http://www.tei-c.org/ns/1.0"
        version="2.0"
        exclude-result-prefixes="#all">
  <!--
  Example command line:

  ant -f ~/Stylesheets/docx/build-from.xml -lib ~/saxonica10/ -DinputFile="test.docx" -DoutputFile="teitest.xml" -DdocxtoTEI="customDocxToTEI.xsl"

  -->
  <xsl:import href="../Stylesheets/profiles/default/docx/from.xsl"/>

  <xsl:template name="elementFromStyle">
    <xsl:param name="style"/>
    <xsl:variable name="poststyle" select="string(if (contains($style,' ')) then
        substring-before($style,' ')
      else if (ends-with($style,'Char')) then
        substring-before($style,'Char')
      else
        $style)"/>
    <xsl:element name="{$poststyle}">
      <xsl:call-template name="basicStyles">
        <xsl:with-param name="parented">true</xsl:with-param>
      </xsl:call-template>
    </xsl:element>
  </xsl:template>

</xsl:stylesheet>
