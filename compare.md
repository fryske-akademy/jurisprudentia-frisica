# tei-publisher behind proxy versus in root

## docker setup with tei-publisher

[see README, Dockerfile and docker-compose.yml](https://bitbucket.org/fryske-akademy/jurisprudentia-frisica)

## apache proxy:

AllowEncodedSlashes NoDecode  
ProxyPass /jurfris http://192.168.178.56:7088/exist/apps/tei-publisher nocanon  
ProxyPassReverse /jurfris http://192.168.178.56:7088/exist/apps/tei-publisher

$config:context-path set to /jurfris

## root

https://teipublisher.com/exist/apps/tei-publisher

## comparison


|                 | behind proxy                                           | in root              |                         |
|---|---|---|---|
| initial load    | 401 on .../api/login                                   | 401 on .../api/login | from checkbox...js:1619 |
|                 |                                                        |                      |                         |
| demo collection | no facets shown                                        | facets shown         |                         |
|                 | empty response for .../api/search/facets |                      |                         |
|                 |                                                        |                      |                         |
| ann. samples -> graves | 404 on ...//login                                      | 404 on ...//login    | from bundle.js:720      |
|                 |                                                        |                      |                         |
| login via box   | ok                                                     | ok                   |                         |
| save annotation | 401 on .../api/annotations/merge/annotate/graves20.xml | ok                   | from annotations.js:270 |
|                 |                                                        |                      |                         |
