# syntax=docker/dockerfile:experimental
FROM existdb/existdb:6.0.0

COPY *.xar $EXIST_HOME/autodeploy/

HEALTHCHECK NONE

RUN --mount=type=secret,id=adminpw,required,target=/tmp/adminpw [ "java", "org.exist.start.Main", "client", "--no-gui",  "-l", "-u", "admin", "-P", "", "-F", "/tmp/adminpw" ]
